       IDENTIFICATION DIVISION.
       PROGRAM-ID. GEN-REPORT.

       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL. 
           SELECT 100-INPUT-FILE ASSIGN TO 'product.csv'
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-100-INPUT-STATUS.

           SELECT 200-INPUT-FILE ASSIGN TO 'transac.csv'
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-200-INPUT-STATUS.   

       DATA DIVISION.
       FILE SECTION. 
       FD  100-INPUT-FILE BLOCK CONTAINS  0 RECORDS.
      *    ASSIGN SIZE FROM PRODUCT.CSV
       01  100-INPUT-RECORD.
           05 100-PRODUCT-ID          PIC X(4)   VALUE SPACE.
           05 FILLER                  PIC X      VALUE SPACE.
           05 100-PRODUCT-NAME        PIC X(15)  VALUE SPACE.
           05 FILLER                  PIC X      VALUE SPACE.        
           05 100-PRODUCT-PRICE       PIC 9V99   VALUE ZERO.

       FD  200-INPUT-FILE BLOCK CONTAINS  0 RECORDS.
      *    ASSIGN SIZE FROM TRANSAC.CSV
       01  200-INPUT-RECORD.
           05 200-TRN-ID              PIC X(5)   VALUE SPACE.
           05 FILLER                  PIC X      VALUE SPACE.
           05 200-YMD-HHMN            PIC X(14)  VALUE SPACE.
           05 FILLER                  PIC X      VALUE SPACE.        
           05 200-PRODUCT-ID          PIC X(5)   VALUE SPACE.
           05 FILLER                  PIC X      VALUE SPACE.
           05 200-QTY                 PIC 9(2)   VALUE ZERO.    

       WORKING-STORAGE SECTION.
       01  WS-CALCULATION.
      *    Size assign from data   
           05 WS-PRODUCT-MAX          PIC 9(3)   VALUE 5.
           05 WS-TRAN-MAX             PIC 9(3)   VALUE 50.

       01  WS-100-INPUT-STATUS        PIC X(2).
           88 FILE-OK                 VALUE '00'.
           88 FILE-AT-END             VALUE '10'.

       01  WS-200-INPUT-STATUS        PIC X(2).
           88 FILE-OK                 VALUE '00'.
           88 FILE-AT-END             VALUE '10'.

      *    OCCURS 5 TIMES FROM PRODUCT.CSV SIZE
       01  WS-ARRAY-PRODUCT  OCCURS 5 TIMES INDEXED BY  WS-ID-PROD.
           05 WSA-PRODUCT-ID          PIC X(4)   VALUE SPACE.
           05 FILLER                  PIC X      VALUE SPACE.
           05 WSA-PRODUCT-NAME        PIC X(15)  VALUE SPACE.
           05 FILLER                  PIC X      VALUE SPACE.        
           05 WSA-PRODUCT-PRICE       PIC 9V99   VALUE ZERO.

      *    OCCURS 5 TIMES FROM TRANSAC.CSV SIZE
       01  WS-ARRAY-TRAN  OCCURS 50 TIMES INDEXED BY  WS-ID-TRN.
           05 WSA-TRN-ID              PIC X(5)   VALUE SPACE.
           05 FILLER                  PIC X      VALUE SPACE.
           05 WSA-YMD-HHMN            PIC X(14)  VALUE SPACE.
           05 FILLER                  PIC X      VALUE SPACE.        
           05 WSA-PRODUCT-ID          PIC X(5)   VALUE SPACE.
           05 FILLER                  PIC X      VALUE SPACE.
           05 WSA-QTY                 PIC 9(2)   VALUE ZERO.    


       PROCEDURE DIVISION.
       000-MAIN-PROGRAM.
           PERFORM 1000-INITIAL THRU  1000-EXIT
           PERFORM 2000-PROCESS THRU  2000-EXIT
           PERFORM 3000-END     THRU  3000-EXIT
           GOBACK 
           .
      
       1000-INITIAL.
           OPEN  INPUT  100-INPUT-FILE 200-INPUT-FILE
           PERFORM 8100-READ-PRODUCT THRU  8100-EXIT
           PERFORM 4100-LOAD-PRODUCT THRU  4100-EXIT
           PERFORM 8200-READ-TRAN THRU  8200-EXIT
           PERFORM 4200-LOAD-TRAN    THRU  4200-EXIT  
           . 
       1000-EXIT.
           EXIT.

       2000-PROCESS.
           DISPLAY 'PROCESS'.
           PERFORM 2910-DEBUG-PRODUCT-ARRAY THRU  2910-EXIT.
           PERFORM 2920-DEBUG-TRAN-ARRAY THRU  2920-EXIT.
       2000-EXIT.
           EXIT.

       2910-DEBUG-PRODUCT-ARRAY.
           PERFORM VARYING WS-ID-PROD FROM  1 BY 1
      *       DO UN TILL THIS UNTILL CASE IS TRUE 
              UNTIL WS-ID-PROD > WS-PRODUCT-MAX
      *       ADD OF TO FIX SAME NAME        
              DISPLAY WSA-PRODUCT-ID OF WS-ARRAY-PRODUCT(WS-ID-PROD )" "
                      WSA-PRODUCT-NAME (WS-ID-PROD) " "
                      WSA-PRODUCT-PRICE (WS-ID-PROD)
           END-PERFORM
           .   
       2910-EXIT.
           EXIT
           .

        2920-DEBUG-TRAN-ARRAY.
           PERFORM VARYING WS-ID-TRN FROM  1 BY 1
      *       DO UN TILL THIS UNTILL CASE IS TRUE 
              UNTIL WS-ID-TRN  > WS-TRAN-MAX      
              DISPLAY WSA-TRN-ID(WS-ID-TRN )" " 
                      WSA-YMD-HHMN(WS-ID-TRN ) " "
      *       ADD OF TO FIX SAME NAME 
                      WSA-PRODUCT-ID OF WS-ARRAY-TRAN(WS-ID-TRN) " "
                      WSA-QTY(WS-ID-TRN ) 
           END-PERFORM
           .   
       2920-EXIT.
           EXIT
           .    

       3000-END.
           CLOSE 100-INPUT-FILE 200-INPUT-FILE .
       3000-EXIT.
           EXIT.

       4100-LOAD-PRODUCT.
           PERFORM VARYING WS-ID-PROD FROM  1 BY 1
      *       DO UN TILL THIS UNTILL CASE IS TRUE 
              UNTIL WS-ID-PROD > WS-PRODUCT-MAX
              OR  FILE-AT-END OF WS-100-INPUT-STATUS

              MOVE  100-INPUT-RECORD  TO WS-ARRAY-PRODUCT (WS-ID-PROD)
              PERFORM 8100-READ-PRODUCT THRU 8100-EXIT 
           END-PERFORM    
           .    
       4100-EXIT.
           EXIT
           .

       4200-LOAD-TRAN.
           PERFORM VARYING WS-ID-TRN  FROM  1 BY 1
      *       DO UN TILL THIS UNTILL CASE IS TRUE 
              UNTIL WS-ID-TRN  > WS-TRAN-MAX
              OR  FILE-AT-END OF WS-200-INPUT-STATUS

              MOVE  200-INPUT-RECORD  TO WS-ARRAY-TRAN  (WS-ID-TRN)
              PERFORM 8200-READ-TRAN  THRU 8200-EXIT 
           END-PERFORM    
           .    
       4200-EXIT.
           EXIT
           .    

       8100-READ-PRODUCT.
           READ 100-INPUT-FILE 
           .
       8100-EXIT.
           EXIT 
           .

        8200-READ-TRAN.
           READ 200-INPUT-FILE 
           .
       8200-EXIT.
           EXIT 
           .                  